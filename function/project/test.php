<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>In this sample is demonstrated how to create a basic navigation
        with jqxTree. </title>
    
    <script type="text/javascript">
        $(document).ready(function () {
            // Create jqxTree
            $("#splitter").jqxSplitter({  width: 1250, height: 500, panels: [{ size: 250}] });
            $('#jqxTree').jqxTree({  height: '100%', width: '100%' });
            $('#jqxTree').css('visibility', 'visible');
            $('#jqxTree').on('select', function (event) {
                $("#ContentPanel").html("<div style='margin: 10px;'>" + event.args.element.id + "</div>");
            });
        });
    </script>
</head>
<body class='default' >
<h2><span class="label label-danger">Quản lý Dự án</span></h2>
    <br>    
    <div style="overflow: hidden; position: relative; margin: auto; width: 1250px;">
        <button title="Thêm" style="float: left; padding: 3px; margin: 2px; width: 25px; height: 25px;" class="jqx-icon-plus"></button>
        <button title="Sửa" style="float: left; padding: 3px; margin: 2px; width: 25px; height: 25px;" class="jqx-icon-edit"></button>
        <button title="Xóa" style="float: left; padding: 3px; margin: 2px; width: 25px; height: 25px;" class="jqx-icon-delete"></button>
        <button title="Hủy" style="float: left; padding: 3px; margin: 2px; width: 25px; height: 25px;" class="jqx-icon-cancel"></button>
        <button title="Lưu" style="float: left; padding: 3px; margin: 2px; width: 25px; height: 25px;" class="jqx-icon-save"></button>
    </div>
    <div id="splitter" style="margin: auto;">
        <div>
            <div style=" border: none;" id='jqxTree'>
                <ul>
                    <li id="Task1" item-expanded='true'>
                        <img style='float: left; margin-right: 5px;' src='images/folder.png' /><span
                            item-title="true">Giai đoạn 1</span>
                        <ul>
                            <li id="Calendar" item-expanded='true'>
                                <img style='float: left; margin-right: 5px;' src='images/settings.png' /><span
                                    item-title="true">Calendar</span> </li>
                            <li id="Contacts">
                                <img style='float: left; margin-right: 5px;' src='images/settings.png' /><span
                                    item-title="true">Contacts</span> </li>
                            <li id="Inbox">
                                <img style='float: left; margin-right: 5px;' src='images/settings.png' /><span
                                    item-title="true"><span>Inbox</span></span>
                            </li>
                        </ul>
                    </li>
                    <li id="Task2" item-expanded='true'>
                        <img style='float: left; margin-right: 5px;' src='images/folder.png' /><span
                            item-title="true">Giai đoạn 2</span>
                        <ul>
                            <li id="Calendar" item-expanded='true'>
                                <img style='float: left; margin-right: 5px;' src='images/settings.png' /><span
                                    item-title="true">Calendar</span> </li>
                            <li id="Contacts">
                                <img style='float: left; margin-right: 5px;' src='images/settings.png' /><span
                                    item-title="true">Contacts</span> </li>
                            <li id="Inbox">
                                <img style='float: left; margin-right: 5px;' src='images/settings.png' /><span
                                    item-title="true"><span>Inbox</span></span>
                            </li>
                        </ul>
                    </li>
                </ul>
                
            </div>
        </div>
        <div>
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Chi tiết</a></li>
                    <li role="presentation"><a href="#timeline" aria-controls="timeline" role="tab" data-toggle="tab">Biểu đồ</a></li>
                    <li role="presentation"><a href="#progress" aria-controls="timeline" role="tab" data-toggle="tab">Tiến độ</a></li>
                    <li role="presentation"><a href="#rate" aria-controls="timeline" role="tab" data-toggle="tab">Đánh giá</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="details">
                    <div id="ContentPanel">
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="timeline">
                    Biểu đồ
                </div>
                <div role="tabpanel" class="tab-pane" id="progress">
                    Tiến độ
                </div>
                <div role="tabpanel" class="tab-pane" id="rate">
                    Đánh giá
                </div>
              </div>
            </div>            
        </div>
    </div>
    <br>
</body>
</html>
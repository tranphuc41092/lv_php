<?php 
	$task=new Task();
	 $projects=$task->find("Task", array());
	  echo '
		<script>
		    var JSONdata = '.json_encode($projects, JSON_UNESCAPED_UNICODE).'
		</script>';
?>
<script>
    var selectedID = "0";

    function findElement(array, id) {
        var element = null;
        array.forEach(function(e) {
            if (e["id"] == id) {
                element = e;
                return;
            }
        });

        return element;
    }

    function showProject(obj) {
        var content = '<button class="btn btn-warning" data-toggle="modal" data-target="#newModal">New Task</button>\
				<button class="btn btn-danger" data-toggle="modal" data-target="#removeModal">Remove</button>\
				<button class="btn btn-info">Edit</button>';

        content += '\
					<table class="table">\
						<tr>\
							<th>Tên dự án</th>\
							<td>' + obj["name"] + '</td>\
						</tr>\
						<tr>\
							<th>Mô tả</th>\
							<td>' + obj["description"] + '</td>\
						</tr>\
						<tr>\
							<th>Ngày bắt đầu</th>\
							<td>' + obj["startdate"] + '</td>\
						</tr>\
						<tr>\
							<th>Ngày kết thúc</th>\
							<td>' + obj["endate"] + '</td>\
						</tr>\
					</table>\
			';
        $('.main').html(content);
    }

    function showTask(obj) {
        var content = '<button class="btn btn-warning" data-toggle="modal" data-target="#newModal">New Task</button>\
				<button class="btn btn-danger" data-toggle="modal" data-target="#removeModal">Remove</button>\
				<button class="btn btn-info">Edit</button>';

        $('.main').html(content);
    }

    $(document).ready(function() {
    	//debugger;

       /* var source = {
            datatype: "json",
            datafields: [{
                name: "id"
            }, {
                name: "belongto"
            }, {
                name: "name"
            }, {
                name: "value"
            }],
            id: "id",
            localdata: JSONdata
        };
        var dataAdapter = new $.jqx.dataAdapter(source);
        dataAdapter.dataBind();
        var records = dataAdapter.getRecordsHierarchy("id", "belongto", "items", [{
            name: "name",
            map: "label"
        }]);
        $("#jqxWidget").jqxTree({
            source: records
        });

        $("#jqxWidget li > div").attr("ng-click", "clickEvent()");

        $("#jqxWidget ul li").click(function(e) {
            e.stopPropagation();
            selectedID = $(this).attr("id");
            var element = findElement(JSONdata, selectedID);

            $("#newForm #belongto").attr("value", selectedID);
            $("#removeForm #belongto").attr("value", selectedID);
            $("#removeForm #name").html(element["name"]);

            switch (element["type"]) {
                case "PROJECT":
                    showProject(element);
                    break;
                case "TASK":
                    showTask(element);
                    break;
            }
        });*/

    });
</script>
<div ng-app="myApp" ng-controller="projectController">
    <br>
    <h2><span class="label label-danger">Quản lý dự án</span></h2>
    <br>
    <br>

    <div class="row">
        <div class="col-md-4 navigation">
            <div id='jqxWidget'>
            </div>
        </div>
        <div class="col-md-8 main">
            <button class="btn btn-warning" data-toggle="modal" data-target="#newModal">New Project</button>
            	<div>
					<table class="table">
						<tr>
							<th>Tên dự án</th>
							<td>{{task.name}}</td>
						</tr>
						<tr>
							<th>Mô tả</th>
							<td>{{task.description}}</td>
						</tr>
						<tr>
							<th>Ngày bắt đầu</th>
							<td>{{task.startdate}}</td>
						</tr>
						<tr>
							<th>Ngày kết thúc</th>
							<td>{{task.enddate}}</td>
						</tr>
					</table>
				</div>
        </div>
    </div>

    <!--New Modal -->
    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New project/task</h4>
                </div>
                <div class="modal-body">
                    <h2><span class="label label-danger">New project/task</span></h2>
                    <br>
                    <br>
                    <form method="post" name="newForm" id="newForm">
                        <div id="create-project">
                            <div class="alert alert-warning" role="alert">
                                <h3><span>Tên dự án:</span></h3>
                                <input type="text" name="name" class="form-control" placeholder="Nhập tên dự án">
                                <br>

                                <h3><span>Yêu cầu của dự án:</span></h3>
                                <textarea type="text" name="description" class="form-control" rows="5" placeholder="Nhập yêu cầu của dự án"></textarea>
                                <br>

                                <h4><span>Thời gian bắt đầu:</span></h4>
                                <input id="startdate" name="startdate" type="date" class="form-control" style="width:40%">
                                <br>

                                <h4><span>Thời gian kết thúc:</span></h4>
                                <input id="enddate" name="enddate" type="date" class="form-control" style="width:40%">

                                <input type="hidden" name="action" value="createTask" />
                                <input type="hidden" id="belongto" name="belongto" value="0" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">OK</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--Remove Modal -->
    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Remove project/task</h4>
                </div>
                <div class="modal-body">
                    <h2><span class="label label-danger">Remove project/task</span></h2>
                    <br>
                    <br>
                    <form method="post" name="removeForm" id="removeForm">
                        <div>
                            <div class="alert alert-warning" role="alert">

                                <h3><span>Do you really want to remove this projec/task?</span></h3>
                                <br>
                                <br>
                                <h4 id="name"></h4>

                                <input type="hidden" name="action" value="removeTask" />
                                <input type="hidden" id="belongto" name="belongto" value="0" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">OK</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

 var source = {
            datatype: "json",
            datafields: [{
                name: "id"
            }, {
                name: "belongto"
            }, {
                name: "name"
            }, {
                name: "value"
            }],
            id: "id",
            localdata: JSONdata
        };
        var dataAdapter = new $.jqx.dataAdapter(source);
        dataAdapter.dataBind();
        var records = dataAdapter.getRecordsHierarchy("id", "belongto", "items", [{
            name: "name",
            map: "label"
        }]);
        $("#jqxWidget").jqxTree({
            source: records
        });

        $("#jqxWidget li > div").attr("ng-click", "clickEvent($event)");

        // $("#jqxWidget ul li").click(function(e) {
        //     e.stopPropagation();
        //     selectedID = $(this).attr("id");
        //     var element = findElement(JSONdata, selectedID);

        //     $("#newForm #belongto").attr("value", selectedID);
        //     $("#removeForm #belongto").attr("value", selectedID);
        //     $("#removeForm #name").html(element["name"]);

        //     switch (element["type"]) {
        //         case "PROJECT":
        //             showProject(element);
        //             break;
        //         case "TASK":
        //             showTask(element);
        //             break;
        //     }
        // });
    var app = angular.module('myApp', []);
    app.controller('projectController', function($scope) {
        $scope.clickEvent = function(event) {
    		var id = event.target.parentNode.id;
    		var obj = findElement(JSONdata, id);
    		$scope.task = obj;
        	 debugger;
        	console.log(g);
        };

    });
</script>